const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("src/pages/dashboardPage.vue")
      },
      {
        name: "edit",
        path: "/edit",
        component: () => import("src/pages/editPage.vue")
      },
      {
        name: "map",
        path: "/addresses",
        component: () => import("src/pages/addressPage.vue")
      },
      {
        name: "message",
        path: "/message",
        component: () => import("src/pages/messagePage.vue")
      },
      {
        name: "wallet",
        path: "/wallet",
        component: () => import("src/pages/walletPage.vue")
      },
      {
        name: "shop",
        path: "/order",
        component: () => import("src/pages/orderPage.vue")
      },
      {
        name: "favorite",
        path: "/favorite",
        component: () => import("src/pages/favoritesPage.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue")
  }
];

export default routes;

import axios from "../../boot/axios";
export default {
  state: {
    favorites: []
  },
  getters: {},
  mutations: {
    setFavorites(state, favorites) {
      state.favorites = favorites;
    }
  },
  actions: {
    getFavorites(context) {
      return axios
        .get("https://fakestoreapi.com/products?limit=8")
        .then((response) => {
          if (response.status === 200) {
            context.commit("setFavorites", response.data);
          }
        })
        .catch((error) => {
          alert(error.message);
        });
    },
    deleteFavorite(context, id) {
      return axios.delete(`https://fakestoreapi.com/products/${id}`);
    }
  }
};

import axios from "../../boot/axios";
export default {
  state: {
    user: {}
  },
  getters: {},
  mutations: {
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    getUser(context) {
      return axios
        .get("https://randomuser.me/api/")
        .then((response) => {
          if (response.status === 200) {
            context.commit("setUser", response.data);
          }
        })
        .catch((error) => {
          alert("Error: " + error.message);
        });
    }
  }
};
